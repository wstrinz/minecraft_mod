## Minecraft
### How 2 MC Forge
- Download src for [v 1.7.10 of MC Forge](http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.7.10.html)
- `./gradlew setupDecompWorkspace --refresh-dependencies`
- `./gradlew eclipse --refresh-dependencies`
- (possibly) edit `eclipse/server.properties` to say `online-mode=false`
- `./gradlew runserver` to run server
- `./gradlew runclient` to run client

#### Troubleshoot
- If invalid session ID, ensure online-mode is false, and try re-authing in vanilla minecraft
